Hỗ trợ các loại mã vạch sau đây:

* UPC-A và UPC-E
* EAN-8 và EAN-13
* Mã 39; 93; 128
* ITF
* Codabar
* RSS-14 (tất cả biến thể)
* Mã QR
* Data Matrix
* Aztec (chất lượng 'beta')
* PDF 417 (chất lượng 'alpha')

Hãy xem trang web để xử lý sự cố và để xem sự giải thích cho các quyền. Ứng dụng này cho phép bạn chia sẻ các liên hệ, ứng dụng, và dấu trang trong một mã QR. Đây là lý do tại sao ứng dụng cần có quyền truy cập danh bạ. Xem "Đi đến trang web của nhà phát triển" phía dưới (https://github.com/zxing/zxing/wiki/Frequently-Asked-Questions).
